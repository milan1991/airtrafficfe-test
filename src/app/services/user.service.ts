import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UserLocation } from '../flights-list/user-location.model';

@Injectable()
export class UserService {

  sharedAircraft;
  userLocation : UserLocation;

  constructor(private http: HttpClient) { }

  /**
   * Send API Get request for aircraft list
   */
  getFlights() {

    let flightsApiUrl = 'https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?trFmt=f&lat=' + this.userLocation.lat + '&lng=' + this.userLocation.lng + '&fDstL=0&fDstU=250';

    return this.http.jsonp(flightsApiUrl, 'callback');

  }

  /**
   * Send API Get request for choosen aircraft logo
   */
  getCompanyLogo() {
    let logoApiUrl = "https://autocomplete.clearbit.com/v1/companies/suggest?query=" + this.sharedAircraft.Op;

    return this.http.get(logoApiUrl);
  }

}
