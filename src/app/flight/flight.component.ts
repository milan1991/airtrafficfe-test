import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  companyName: string;
  manufacturer: string;
  model: string;
  destination: string;
  origin: string;
  logo: string = '/assets/images/air-transport-black.png';
  companyWebsite: string;

  constructor(private userServ : UserService, private location: Location) {
    this.companyName = userServ.sharedAircraft.Op || "Unknown";
    this.manufacturer = userServ.sharedAircraft.Man || "Unknown";
    this.model = userServ.sharedAircraft.Mdl || "Unknown";
    this.destination = userServ.sharedAircraft.To || "Unknown";
    this.origin = userServ.sharedAircraft.From || "Unknown";
  }

  ngOnInit() {
    this.userServ.getCompanyLogo().subscribe(
      (res: Array<any>)=>{
        if (res.length) {
          this.logo = res[0].logo;
          this.companyWebsite = res[0].domain;
        }
      }, (err)=>{
        //console.log(err)
      }
    );
  }

  /**
   * Returns user back to flights-list component
   */
  onClickHomeButton(){
    this.location.back();
  }

}
