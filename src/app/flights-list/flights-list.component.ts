import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { UserService } from '../services/user.service'
import { UserLocation } from './user-location.model';

@Component({
  selector: 'app-flights-list',
  templateUrl: './flights-list.component.html',
  styleUrls: ['./flights-list.component.css']
})

export class FlightsListComponent implements OnInit, OnDestroy {
  
  flightList = []; // All plights from server
  loading: boolean = false; // Loading data (spinner)
  error: string = ''; // Error message
  info: string = ''; // Info Message
  subscription : Subscription; // Subscribtion

  constructor(private userServ: UserService, private router: Router) {
  }

  ngOnInit() {
    if(!this.userServ.sharedAircraft) {
      this.getCurrentPosition();
    } else {
      this.getDataFromServer();
    }
  }

  /**
   * Flights sorting function (altitude DESC)
   */
  sortFlights(arr) {
    const sorted = arr.sort((a, b) => a['Alt'] < b['Alt'] ? 1 : a['Alt'] === b['Alt'] ? 0 : -1);
    return sorted;
  }

  /**
   * Geolocation handling function.
   * If browser supports geolocation and user allow it, get data from server.
   * Else show error info message.
   */
  getCurrentPosition() {
    this.error = '';
    this.info = "Waiting for geolocation...";
    
    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        (position) => {
          this.info = '';

          this.userServ.userLocation = new UserLocation(position.coords.latitude, position.coords.longitude, position.timestamp);
          this.getDataFromServer();  //send position to backend

        }, (error) => {
          this.info = '';

          switch (error.code) {
            case 3:
              // ...timeout
              this.error = 'Geolocation could not find you, please try again later.';
              break;
            case 2:
              // ...device can't get data
              this.error = 'Geolocation could not find you, please try again later.';
              break;
            case 1:
              this.error = 'You must allow geolocation to use this app.';
          }

        });
      } else {
        this.error = 'We are sorry, geolocation is not supported in this browser.';
      }
  }

  /**
   * GET data from API based on user location.
   * If server not responding show error info message.
   */
  getDataFromServer() {
    this.loading = true;
    this.error = '';
    this.info = 'Loading data from server...';

    this.subscription = this.userServ.getFlights().subscribe(
      (res)=>{
        this.loading = false;
        this.info = '';

        this.flightList = this.sortFlights(res['acList']);
        
        setTimeout(()=>{this.getDataFromServer()}, 60000); // Load data from server after minut
      }, (err)=>{
        this.loading = false;
        this.info = '';
        this.error = 'Server not responding, try again later.';
      }
    )
  }

  /**
   * Handling click on table row (flight) and redirects user to flight component (details)
   */
  onAircraftClick(aircraft) {
    this.userServ.sharedAircraft = aircraft;
    this.router.navigate(['flight/', aircraft.Id]);
  }

  /**
   * Destroying existing subscriptions.
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

