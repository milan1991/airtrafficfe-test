export class UserLocation {
    lat: number;
    lng: number;
    timestamp: number;
  
    constructor(lat, lng, timestamp) {
      this.lat = lat;
      this.lng = lng;
      this.timestamp = timestamp;
    }
  }