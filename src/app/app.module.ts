import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { AppRoutingModule, RouteGuard } from './app-routing.module';
import { UserService } from './services/user.service'

import { AppComponent } from './app.component';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { FlightComponent } from './flight/flight.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    FlightsListComponent,
    FlightComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HttpClientJsonpModule
  ],
  providers: [UserService, RouteGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
