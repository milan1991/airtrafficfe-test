import { NgModule, Injectable } from '@angular/core';
import { RouterModule, Routes, CanActivate, Router }  from '@angular/router';
 
import { FlightsListComponent }   from './flights-list/flights-list.component';
import { FlightComponent }     from './flight/flight.component';
import { UserService } from './services/user.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
 
@Injectable()
export class RouteGuard implements CanActivate {
  constructor(private userServ: UserService, private router: Router) {};
  canActivate() {
    if(!this.userServ.userLocation || !this.userServ.sharedAircraft) {

      this.router.navigate(['flights-list']);
      
      return false;
    } else {
      return true;
    }
  }
}


const appRoutes: Routes = [
    {
        path: 'flight/:id',
        component: FlightComponent,
        canActivate: [RouteGuard]
    },
    {
        path: 'flights-list',
        component: FlightsListComponent
    },
    {
        path: '',  
        redirectTo: '/flights-list', pathMatch: 'full'
    },
    { path: '**', component: PageNotFoundComponent }
];
 
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
